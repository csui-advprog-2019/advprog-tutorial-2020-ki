package id.ac.ui.cs.advprog.tutorial7.strategy.core;

/**
* Obviously not a dummy class
* Note : actually it is, you can delete this class after completing every task for Strategy Pattern
*/
public class DummyAction implements AttackAction, DefenseAction, SupportAction {
	
	public String getDescription(){
		return "Do nothing";
	}
	
	public String attack(){
		return "Do nothing";
	}
	
	public String defense(){
		return "Do nothing";
	}
	
	public String support(){
		return "Do nothing";
	}
}