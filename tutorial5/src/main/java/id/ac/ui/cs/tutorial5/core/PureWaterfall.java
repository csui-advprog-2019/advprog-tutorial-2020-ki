package id.ac.ui.cs.tutorial5.core;

import id.ac.ui.cs.tutorial5.service.RandomizerService;

public class PureWaterfall extends Craftable {

    public PureWaterfall() {
        super("Pure Waterfall", RandomizerService.getRandomCostValue());
    }

    @Override
    public String getDoneCraftMessage() {
        return "Water comes from the highland, pure blue in color \n";
    }
}
