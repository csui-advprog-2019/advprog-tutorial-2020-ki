package id.ac.ui.cs.tutorial5.service;

import id.ac.ui.cs.tutorial5.core.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CraftServiceImpl implements CraftService {

    private String[] itemName = {
            "Dragon Breath", "Void Rhapsody", "Determination Symphony",
            "Opera of Wasteland", "FIRE BIRD"
    };

    private  List<CraftItem> allItems = new ArrayList<>();

    @Override
    public CraftItem createItem(String itemName) {
        CraftItem craftItem;
        switch (itemName) {
            case "Dragon Breath":
                craftItem = createDragonBreath();
                break;
            case "Void Rhapsody":
                craftItem = createVoidRhapsody();
                break;
            case "Determination Symphony":
                craftItem = createDeterminationSymphony();
                break;
            case "Opera of Wasteland":
                craftItem = createOperaOfWasteland();
                break;
            case "FIRE BIRD":
                craftItem = createFireBird();
                break;
            default:
                craftItem = createFireBird();
        }
        craftItem.composeRecipes();
        allItems.add(craftItem);
        return craftItem;
    }

    // @TODO convert all creational method to async using CompleteableFuture
    // @TODO Based on Design principe DRY, fix code duplication from running async
    private CraftItem createDragonBreath() {
        CraftItem dragonBreath = new CraftItem("Dragon Breath");
        dragonBreath.addRecipes(new SilentField());
        dragonBreath.addRecipes(new FireCrystal());
        return dragonBreath;
    }

    private CraftItem createVoidRhapsody() {
        CraftItem voidRhapsody = new CraftItem("Void Rhapsody");
        voidRhapsody.addRecipes(new SilentField());
        voidRhapsody.addRecipes(new PureWaterfall());
        return voidRhapsody;
    }

    private CraftItem createDeterminationSymphony() {
        CraftItem determinationSymphony = new CraftItem("Determination Symphony");
        determinationSymphony.addRecipes(new BlueRoseRainfall());
        determinationSymphony.addRecipes(new PureWaterfall());
        return determinationSymphony;
    }

    private CraftItem createOperaOfWasteland() {
        CraftItem wasteland = new CraftItem("Opera of Wasteland");
        wasteland.addRecipes(new DeathSword());
        wasteland.addRecipes(new SilentField());
        wasteland.addRecipes(new FireCrystal());
        return wasteland;
    }

    private CraftItem createFireBird() {
        CraftItem fireBird =  new CraftItem("FIRE BIRD");
        fireBird.addRecipes(new FireCrystal());
        fireBird.addRecipes(new BirdEggs());
        return fireBird;
    }

    @Override
    public String[] getItemNames() {
        return itemName;
    }

    @Override
    public List<CraftItem> findAll() {
        return allItems;
    }
}
