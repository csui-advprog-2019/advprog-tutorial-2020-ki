package id.ac.ui.cs.tutorial9.factory.core.parts;

public interface Spell {
    String cast();
}