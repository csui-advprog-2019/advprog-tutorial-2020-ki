package id.ac.ui.cs.tutorial9.factory.core.parts;

public class EnhanceSpeed implements Spell {
    public String cast(){
        return "Enhance Speed : Enhance the knight's action and reaction speed";
    }
}