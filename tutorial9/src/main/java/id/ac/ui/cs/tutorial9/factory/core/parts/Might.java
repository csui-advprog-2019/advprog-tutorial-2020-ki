package id.ac.ui.cs.tutorial9.factory.core.parts;

public class Might implements Affinity {
    public String getDescription(){
        return "Might : This knight assert dominance by demonstrating their might";
    }
}