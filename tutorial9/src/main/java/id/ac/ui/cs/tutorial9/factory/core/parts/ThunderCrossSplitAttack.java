package id.ac.ui.cs.tutorial9.factory.core.parts;

public class ThunderCrossSplitAttack implements Affinity, Aura, Spell, Weapon {
    private static String everything = "Ha! You fell for it, fool. THUNDER CROSS SPLIT ATTACK";

    public String getDescription(){
        return everything;
    }

    public String activate(){
        return everything;
    }

    public String cast(){
        return everything;
    }

    public String attack(){
        return everything;
    }
}