package id.ac.ui.cs.tutorial10.service;

import id.ac.ui.cs.tutorial10.model.MagicModel;
import id.ac.ui.cs.tutorial10.repository.MagicRepository;
import reactor.core.publisher.Flux;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MagicServiceImpl implements MagicService {

    @Autowired
    private MagicRepository repo;

    public Flux<MagicModel> findAll(){
        //ToDo: Implement this. Make sure to return a Flux<MagicModel> reactive stream.
        return null;
    }

    public MagicModel addMagic(MagicModel magicModel){
        //ToDo: Implement this.
        return null;
    }

}