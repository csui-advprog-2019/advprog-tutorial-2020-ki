package id.ac.ui.cs.tutorial10.repository;

import id.ac.ui.cs.tutorial10.model.MagicModel;
import id.ac.ui.cs.tutorial10.enumattr.MagicType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MagicRepository extends JpaRepository<MagicModel, Long> {
    MagicModel findById(@Param("id") long id);
    MagicModel findByName(@Param("name") String name);
    List<MagicModel> findByType(@Param("type") MagicType type);
}