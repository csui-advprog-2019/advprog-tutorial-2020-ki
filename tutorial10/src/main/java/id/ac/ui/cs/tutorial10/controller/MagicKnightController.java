package id.ac.ui.cs.tutorial10.controller;

import id.ac.ui.cs.tutorial10.model.MagicKnightModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import id.ac.ui.cs.tutorial10.service.MagicKnightService;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/api/magicknight")
public class MagicKnightController {

    @Autowired
    MagicKnightService magicKnightService;

    @GetMapping("/")
    public Flux<MagicKnightModel> getAllMagicKnights() {
        //ToDo: Implement this.
        return null;
    }

    @PostMapping("/create/")
    public ResponseEntity createMagic(@RequestBody MagicKnightModel magicKnightModel) {
        //ToDo: Implement this.

        return ResponseEntity.ok("Added new Magic Knight");
    }

}