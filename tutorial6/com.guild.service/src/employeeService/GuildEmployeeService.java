package employeeService;

import filemanager.FileManager;
import guildEmployee.GuildStaff;

import java.util.List;

public class GuildEmployeeService {
    private FileManager<GuildStaff> guildStaffFileManager = new FileManager<>("Guild.json", GuildStaff.class);

    public List<GuildStaff> findAll() {
        return guildStaffFileManager.getAllDataFromFIle();
    }

    public List<GuildStaff> findByName(String name) {
        List<GuildStaff> allStaf = findAll();
        allStaf.removeIf(guildStaff-> !(guildStaff.getName().equalsIgnoreCase(name)) );
        return allStaf;
    }

    public GuildStaff create(String name, String position) {
        GuildStaff newStaff = new GuildStaff();
        setBasicInformation(name, position, newStaff);
        setWageInformation(position, newStaff);
        guildStaffFileManager.writeToFileAsJson(newStaff);
        return newStaff;
    }

    private void setWageInformation(String position, GuildStaff newStaff) {
        switch (position) {
            case "Master":
                newStaff.setWage(200000);
                break;
            case "Clerk":
                newStaff.setWage(80000);
                break;
            default:
                newStaff.setWage(10000);
                newStaff.setPosition("Staff");
                break;
        }
    }

    private void setBasicInformation(String name, String position, GuildStaff newStaff) {
        newStaff.setName(name);
        newStaff.setId(name+position);
        newStaff.setPosition(position);
    }
}
