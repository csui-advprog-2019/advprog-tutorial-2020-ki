package id.ac.ui.cs.advprog.tutorial8.adapter.core;

public class MagicResponseWrapper {

    private String magicName;
    private String cast;

    public String getMagicName() {
        return magicName;
    }

    public void setMagicName(String magicName) {
        this.magicName = magicName;
    }

    public String getCast() {
        return cast;
    }

    public void setCast(String cast) {
        this.cast = cast;
    }
}
