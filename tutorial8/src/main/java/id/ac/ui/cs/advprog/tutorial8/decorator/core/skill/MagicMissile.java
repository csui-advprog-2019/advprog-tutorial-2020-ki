package id.ac.ui.cs.advprog.tutorial8.decorator.core.skill;

public class MagicMissile extends Skill {
        public MagicMissile() {
                skillName = "Magic Missile";
                skillDescription = "Tembak mejik gan";
                skillPower = 150;
                skillManacost = 200;
        }
}
