package id.ac.ui.cs.advprog.tutorial8.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial8.decorator.core.skill.Skill;

import java.util.Random;

public class PowerUpgrade extends Skill {

    Skill skill;

    public PowerUpgrade(Skill skill) {

        this.skill = skill;
    }

    @Override
    public String getName() {
        return skill.getName();
    }

    // Powers up skill power
    @Override
    public int getSkillPower() {
        //ToDo: Complete me
        return 0;
    }

    @Override
    public String getDescription() {
        return this.skill.getDescription() + " + powered up";
    }
}
