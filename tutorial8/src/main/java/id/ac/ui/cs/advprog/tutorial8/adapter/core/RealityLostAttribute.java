package id.ac.ui.cs.advprog.tutorial8.adapter.core;

public class RealityLostAttribute {

    public void changeReality() {
        //  This section are beyond human understanding
    }
    public String attributeSacriface() {
        return "999999";
    }

    public String attributeCalling() {
        return "Reality Manipulation Lost Attribute";
    }
}
